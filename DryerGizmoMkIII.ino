#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
int lcd_key     = 0;
int adc_key_in  = 0;
int hrs = 0;
int mins = 0;
int set_mins = 0;
int set_hrs = 1;
int secs = 60;
int cursor_pos = 1;
int spin_pin = 11;
int heater_pin = 12;

int cooldown = 0;

bool startTimer = false;
bool setTimer = true;
bool get_time = false;

unsigned long interval=1000; // the time we need to wait
unsigned long previousMillis=0; // millis() returns an unsigned long.

#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

int read_LCD_buttons()
{
 adc_key_in = analogRead(0);      // reading the button value from the lcd keypad
 // checking which button is pressed
 if (adc_key_in > 1000) return btnNONE; // We make this the 1st option for speed reasons since it will be the most likely result
 if (adc_key_in < 50)   return btnRIGHT;  
 if (adc_key_in < 250)  return btnUP; 
 if (adc_key_in < 450)  return btnDOWN; 
 if (adc_key_in < 650)  return btnLEFT; 
 if (adc_key_in < 850)  return btnSELECT;  

 return btnNONE;  // when all others fail, return this...
}

void setup()
{
 Serial.begin(115200);
 pinMode(spin_pin, OUTPUT);
 pinMode(heater_pin, OUTPUT);
 digitalWrite(spin_pin, HIGH);
 digitalWrite(heater_pin, HIGH);
 
 lcd.begin(16, 2);              // start communication with LCD keypad shield
 lcd.setCursor(0,0);
 lcd.print("Who's that bean?");
 lcd.setCursor(0, 1);
 lcd.print("Its Gizmo mk.III!");
 delay(3000);
}
 
void loop(){
  // Checking which condition is true based on the button pressed
  if(startTimer == true){
    start_timer();
  }
  else if (setTimer == true){
    set_timer();
  }
}

// This function will count the time and will beep the buzzer when the time will be up.
void start_timer(){
  // Checking whether time is up or not
  if(hrs == 0 && mins == 0 && secs == 0){
    lcd.setCursor(0, 0);
    lcd.print(" Dryer DONE!");
    lcd.setCursor(0, 1);
    lcd.print("  beep beep!!");
    digitalWrite(spin_pin, HIGH);
    delay(500);
    digitalWrite(heater_pin, HIGH);
    delay(500);
  }
  
  else if(hrs == 0 && mins < 10 && cooldown == 0){
    cooldown = 1;
    digitalWrite(heater_pin, HIGH);
    delay(500);
  }
  
  else if(secs < 0){
    secs = 59;
    mins = mins - 1;
  }

  else if(mins < 0){
    mins = 59;
    hrs = hrs - 1;
  }

  else
  {
  get_time = true;
  counter();
  if (cooldown == 1){
  lcd.setCursor(0, 0);
  lcd.print("COOLING DOWN");
  }
  else
  {
  lcd.setCursor(0, 0);
  lcd.print("DRYER IS ON ");
  }
  
  lcd.setCursor(0, 1);
  lcd.print(hrs);
  lcd.print(":");

  lcd.setCursor(4, 1);
  lcd.print(mins);
  lcd.print(":");
  
  lcd.setCursor(8, 1);
  lcd.print(secs);
  }
   
  lcd_key = read_LCD_buttons();  // read the buttons

  switch (lcd_key)               // depending on which button was pushed, we perform an action
  {
    // if select button is pressed, move back to set time
   case btnSELECT:
     {
      startTimer = false;
      setTimer = true;
      delay(300);
      lcd.clear();
     break;
     }
   case btnNONE:
     {
     break;
     }
 }
}

// This function will set the time
void set_timer(){
  counter();
  lcd.setCursor(0, 0);
  lcd.print("Timer SETUP");

  lcd.setCursor(0, 1);
  lcd.print("Hrs:");
  lcd.print(hrs);
  
  lcd.setCursor(8, 1);
  lcd.print("Mins:");
  lcd.print(mins);
  

 lcd.setCursor(0,1);
 lcd_key = read_LCD_buttons();  // read the buttons

 switch (lcd_key)               // depending on which button was pushed, we perform an action
 {
  // if right button is pressed, then move the cursor to minutes
   case btnRIGHT:
     {
      cursor_pos = set_mins;
      lcd.setCursor(15,1);
      lcd.print("<");
     break;
     }
  // if left button is pressed, then move the cursor to hours
   case btnLEFT:
     {
      cursor_pos = set_hrs;
      lcd.setCursor(5,1);
      lcd.print("<");
     break;
     }
  // if up button is pressed, add 1 to the minutes or hours
   case btnUP:
     {
      delay(300);
      if(cursor_pos == set_mins){
        mins += 10;
        if(mins > 50){
          mins = 0;
        }
      }
      else if(cursor_pos == set_hrs){
        hrs += 1;
        if(hrs > 2){
          hrs = 0;
        }
      }
     break;
     }
   // if down button is pressed, minus 1 from the minutes or hours 
   case btnDOWN:
     {
      delay(300);
      if(cursor_pos == set_mins){
        mins -= 10;
        if(mins < 0){
          mins = 50;
        }
      }
      else if(cursor_pos == set_hrs){
        hrs--;
        if(hrs < 0){
          hrs = 2;
        }
      }
     break;
     }
   // if select button is pressed, start the timer
   case btnSELECT:
     {
      startTimer = true;
      setTimer = false;
      mins = mins - 1;
      delay(300);
  digitalWrite(heater_pin, LOW);
  digitalWrite(spin_pin, LOW);
     break;
     }
   case btnNONE:
     {
     break;
     }
 }
}

void counter() {
 unsigned long currentMillis = millis(); // grab current time
 
 // check if "interval" time has passed (1000 milliseconds)
 if ((unsigned long)(currentMillis - previousMillis) >= interval) {
  
  lcd.clear();
  if(get_time == true){
   secs--;
   get_time = false;
  }
   previousMillis = millis();
 }
}