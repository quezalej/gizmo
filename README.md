# Clothes Dryer Gizmo

## Background
The clothes dryer in the place I was living in previously stopped working, so I decided to fix it. After inspecting the appliance, I found it was the timer dial which was causing the issue. I disassembled it and found that the timer acts like a clock with rings instead of hands, which push the spring relays to turn them on, and counts down via a 120v powered "clock".


Here is an example of a similar timer dial (picture taken by D Rogers, from https://www.instructables.com/Test-and-Repair-a-clothes-dryer-switch/):
!["Dryer timer mechanism"](IMAGES/switch.jpg)

After searching for a replacement part and finding it would end up being more expensive than what we paid for the dryer itself, I decided to invent a digital alternative that could provide the same functionality without compromising the safety and reliability of the appliance. (The electricity that powers the 240V spinning motor+fans and the heating element runs through the timer relays, this was the cause of the failure as the heat produced at the contact points melted part of the housing rendering the springs useless).

## Construction and functionality
For this project I used an Arduino UNO board fitted with a LCD+Keypad shield, this provided me with an easy way to display information and receive input. The user is able to input a time in hours and minutes using the directional buttons on the keypad and confirm the decision with the Enter button. The Arduino board is controlling a high power 30A 2 channel relay module which powers both the heating element and fan+motor circuits. Trying to replicate as accurately as possible the features of the original timer I limited the maximum cycle to about 2 hours and included a cool down period during the last 10 minutes of the cycle during which only the fan+motor kept running while the heating element is off. After the cycle is completed a message is displayed on the screen and the dryer stops, a new cycle can be started after pressing the reset button on the keypad. A phone charger that provides 5V usb was used to power the Arduino board, the high power relay module also requires 120V power to function so both of these devices were wired to the contacts that powered the original timer dial mechanism.

- Original timer control

!["Timer"](IMAGES/timer.jpg)

- Arduino relay control

!["Arduino"](IMAGES/arduino.jpg)

## Sketch information
The Arduino [sketch](DryerGizmoMkIII.ino) provides a user interface on the LCD screen, reading user input from the keypad buttons and updating the information on the screen to reflect the settings selected. Once a cycle is confirmed, the status and time left is displayed on the screen. While the cycle is running, the Arduino signals the relay module to turn on both channels. When it detects that the time left is less than 10 minutes, it turns off the heater circuit, leaving the fan and motor running for a cool down period. After the cycle is completed, both relay channels are turned off, and the program does not allow them to be turned on until manually reset using a button on the keypad.

## Pictures
- Gizmo waiting for user input

![Gizmo](IMAGES/gizmo.jpg)

- Short video of Gizmo after installation, startup sequence

![Gizmo video](IMAGES/gizmo.mp4)



